package com.example.twicediceroller

/**
 * @author Yusuf Tanrıkulu
 */
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.ImageView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val rollButton: Button = findViewById(R.id.button)
        rollButton.setOnClickListener {
            rollTwoDices()
        }
        // It dices initially
        rollTwoDices()
    }

    /**
     * Roll Two dices and update the screen with result
     */
    private fun rollTwoDices() {
        val dice = Dice(6)

        val firstDiceImage: ImageView = findViewById(R.id.imageView5)
        val secondDiceImage: ImageView = findViewById(R.id.imageView6)

        val firstDiceImageResource = dice.getDrawableResources(dice.roll())
        val secondDiceImageResource = dice.getDrawableResources(dice.roll())

        firstDiceImage.setImageResource(firstDiceImageResource)
        secondDiceImage.setImageResource(secondDiceImageResource)

    }
}

/**
 * Creates Dice class and roll function
 */
class Dice(private val numSides: Int) {

    /**
     * return 1 - 6 dice number randomly
     */
    fun roll(): Int {
        return (1..numSides).random()
    }

    /**
     * return dice image resource according to dice number
     */
    fun getDrawableResources(diceNumber: Int): Int {
        return when (diceNumber) {
            1 -> R.drawable.dice_1
            2 -> R.drawable.dice_2
            3 -> R.drawable.dice_3
            4 -> R.drawable.dice_4
            5 -> R.drawable.dice_5
            else -> R.drawable.dice_6
        }
    }
}
